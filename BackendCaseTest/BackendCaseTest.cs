using Microsoft.VisualStudio.TestTools.UnitTesting;
using backendCase;
using System.Collections.Generic;

namespace BackendCaseTest;

[TestClass]
public class BackendCaseTest
{
    [TestMethod]
    public void AddItem()
    {
        BackendSystem backendSystem = new BackendSystem();
        Item book = new Book(0, "Texts from Denmark", new List<string>(){"Brian Jensen"}, "Gyldendal", 2001, 253);
        
        bool expected = true;
        bool actual = backendSystem.AddItem(book);
        
        Assert.AreEqual(expected, actual);
    }

    [TestMethod]
    public void FindBooks()
    {
        BackendSystem backendSystem = new BackendSystem();
        Book bookA = new Book(0, "Texts from Denmark", new List<string>(){"Brian Jensen"}, "Gyldendal", 2001, 253);
        Book bookB = new Book(1, "Stories from abroad", new List<string>(){"Peter Jensen", "Hans Andersen"}, "Borgen", 2012, 156);
        backendSystem.AddItem(bookA);
        backendSystem.AddItem(bookB);

        List<Book> expected = new List<Book>(){bookA, bookB};
        List<Book> actual = backendSystem.FindBooks("*20*");

        CollectionAssert.AreEqual(expected, actual);

        expected = new List<Book>(){bookB};
        actual = backendSystem.FindBooks("*20* & *Peter*");

        CollectionAssert.AreEqual(expected, actual);
    }

    [TestMethod]
    public void GetItemLocation()
    {
        BackendSystem backendSystem = new BackendSystem();
        Book bookA = new Book(0, "Texts from Denmark", new List<string>(){"Brian Jensen"}, "Gyldendal", 2001, 253);
        Book bookB = new Book(1, "Stories from abroad", new List<string>(){"Peter Jensen", "Hans Andersen"}, "Borgen", 2012, 156);
        backendSystem.AddItem(bookA);
        backendSystem.AddItem(bookB);

        string expected = "0-0-0";
        string actual = backendSystem.GetItemLocation(0);

        Assert.AreEqual(expected, actual);

        expected = "0-0-1";
        actual = backendSystem.GetItemLocation(1);

        Assert.AreEqual(expected, actual);
    }

    [TestMethod]
    public void GetInventory()
    {
        BackendSystem backendSystem = new BackendSystem();
        Book    bookA   = new Book   (0, "Texts from Denmark",        new List<string>(){"Brian Jensen"}, "Gyldendal", 2001, 253);
        Book    bookB   = new Book   (1, "Stories from abroad",       new List<string>(){"Peter Jensen", "Hans Andersen"}, "Borgen", 2012, 156);
        Dvd     dvd     = new Dvd    (2, "Titanic",                   new List<Track>(){new Track(TrackType.video, "Titanic", "James Cameron", 97)});
        Cd      cd      = new Cd     (3, "Nightwish",                 new List<Track>(){new Track(TrackType.audio, "The Greates Show On Earth", "Nightwish", 60)});
        Blueray blueray = new Blueray(4, "Avatar - The way of water", new List<Track>(){new Track(TrackType.video, "Avatar - The way of water", "James Cameron", 192)});
        backendSystem.AddItem(bookA);
        backendSystem.AddItem(bookB);
        backendSystem.AddItem(dvd);
        backendSystem.AddItem(cd);
        backendSystem.AddItem(blueray);

        List<Item> expected = new List<Item>(){bookA, bookB, dvd, cd, blueray};
        List<Item> actual = backendSystem.GetInventory();

        CollectionAssert.AreEqual(expected, actual);

        expected = new List<Item>(){bookA, bookB, dvd, cd};
        actual = backendSystem.GetInventory("0");

        CollectionAssert.AreEqual(expected, actual);

        expected = new List<Item>(){blueray};
        actual = backendSystem.GetInventory("1");

        CollectionAssert.AreEqual(expected, actual);

        expected = new List<Item>(){bookA, bookB};
        actual = backendSystem.GetInventory("0-0");

        CollectionAssert.AreEqual(expected, actual);
        
        expected = new List<Item>(){dvd, cd};
        actual = backendSystem.GetInventory("0-1");

        CollectionAssert.AreEqual(expected, actual);

        expected = new List<Item>(){bookA};
        actual = backendSystem.GetInventory("0-0-0");

        CollectionAssert.AreEqual(expected, actual);

        expected = new List<Item>(){bookB};
        actual = backendSystem.GetInventory("0-0-1");

        CollectionAssert.AreEqual(expected, actual);
    }
}