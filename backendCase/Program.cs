﻿namespace backendCase
{
/*
Problem
    A library has a large collection of books organized in different rooms, rows and book-shelves that are all numbered.
    The local librarian is working on an application in which he can register the books and make inventory lists for a given room, row or book-shelf.
    Also it must be possible to locate a given book by its ISBN number.

Exercise
    - Create an object model that is able to represent the necessary entities.
    - Establish a small set of test data.
    - Add necessary object collections to enable efficient creation of the inventory lists and locating books.
    - Create objects and populate collections with the test data. Insertion speed of new books is not relevant.
    - Establish a set of test-cases that verifies the solution against the test data.
*/
    class Program
    {
        static void Main()
        {
            BackendSystem backendSystem = new BackendSystem();
            Book    bookA   = new Book   (0, "Texts from Denmark",        new List<string>(){"Brian Jensen"}, "Gyldendal", 2001, 253);
            Book    bookB   = new Book   (1, "Stories from abroad",       new List<string>(){"Peter Jensen", "Hans Andersen"}, "Borgen", 2012, 156);
            Dvd     dvd     = new Dvd    (2, "Titanic",                   new List<Track>(){new Track(TrackType.video, "Titanic", "James Cameron", 97)});
            Cd      cd      = new Cd     (3, "Nightwish",                 new List<Track>(){new Track(TrackType.audio, "The Greates Show On Earth", "Nightwish", 60)});
            Blueray blueray = new Blueray(4, "Avatar - The way of water", new List<Track>(){new Track(TrackType.video, "Avatar - The way of water", "James Cameron", 192)});
            backendSystem.AddItem(bookA);
            backendSystem.AddItem(bookB);
            backendSystem.AddItem(dvd);
            backendSystem.AddItem(cd);
            backendSystem.AddItem(blueray);

            Console.WriteLine(backendSystem.GetItemLocation(0));
            Console.WriteLine(backendSystem.GetItemLocation(1));
            Console.WriteLine(backendSystem.GetItemLocation(2));

            Console.WriteLine("\n\x1b[1mThis is the current inventory\x1b[0m");

            List<Item> inventory = backendSystem.GetInventory(); // TODO: Needs to include other types of items other than books
            foreach(Item item in inventory)
                Console.WriteLine(item.ToString());

            Console.WriteLine("\n\x1b[1mFirst search for books containing 20 in the title, author, publisher or in the publication year\x1b[0m");

            List<Book> result = backendSystem.FindBooks("*20*");
            foreach(Book book in result)
                Console.WriteLine(book);

            Console.WriteLine("\n\x1b[1mSecond search for books containing both '20' and 'Peter' in the title, author, publisher or in the publication year\x1b[0m");

            result = backendSystem.FindBooks("*20* & *Peter*");
            foreach(Book book in result)
                Console.WriteLine(book);
        }
    }
}