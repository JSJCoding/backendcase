namespace backendCase
{
    public interface IBackendSystem
    {
        bool AddItem(Item item);
        string GetItemLocation(int ISBN);
        List<Item> GetInventory(string location);
        List<Book> FindBooks(string searchString);
    }
}