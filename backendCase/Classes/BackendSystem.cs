namespace backendCase
{
    public class BackendSystem : IBackendSystem
    {
        private List<Item> items = new List<Item>();
        private List<Room> rooms = new List<Room>();
        private int ISBN = 0; // Note: This is really not needed, when Items are added to the system it already has a known ISBN number, that needs to be given.
        private int roomID = 0;

        public BackendSystem()
        {
            // Read the content stored in the database, this includes all items, here: book, CD, and others, but also the rooms, the number of bookselves and the number of rows in the selves.
            // items.Add(new Book(ISBN++, "Texts from Denmark", new List<string>(){"Brian Jensen"}, "Gyldendal", 2001, 253));
            // items.Add(new Book(ISBN++, "Stories from abroad", new List<string>(){"Peter Jensen", "Hans Andersen"}, "Borgen", 2012, 156));
        }

        // Note to self: I believe there is a better way to structure the Room -> Bookshelv -> Row structure for faster insert.
        public bool AddItem(Item item)
        {
            if(roomID == 0)
                rooms.Add(new Room(roomID++));

            bool status = rooms[roomID-1].AddToInventory(item);
            if(status == false)
            {
                rooms.Add(new Room(roomID++));
                status = rooms[roomID-1].AddToInventory(item);
            }
            
            items.Add(item);
            return status;
        }

        public List<Book> FindBooks(string searchString)
        {
            searchString = searchString.Replace(" ", string.Empty);
            searchString = searchString.Replace("*", string.Empty);
            string[] searchTerms = searchString.Split("&"); // TODO: Need to be able to differenciate between \& and &

            List<Book> result = new List<Book>();
            foreach(Item item in items)
            {
                if(item is not Book)
                    continue;
                
                if(!item.ContainsAll(searchTerms))
                    continue;

                result.Add((Book)item);
            }

            return result;
        }

        public string GetItemLocation(int ISBN)
        {
            Item item = items.FirstOrDefault(item => item.ISBN == ISBN);
            if(item != null)
                return item.location; // I might want to implement location for all items. Putting the propperty in the Item class.
            return "No item found!";
        }

        public List<Item> GetInventory(string location = "default")
        {
            List<Item> inventory = new List<Item>();
            List<int> args = new List<int>();

            if(location != "default")
                foreach(string arg in location.Split("-"))
                    args.Add(int.Parse(arg));

            switch(args.Count())
            {
                case 1: inventory = rooms[args[0]].GetInventory(); break;
                case 2: inventory = rooms[args[0]].bookshelves[args[1]].GetInventory(); break;
                case 3: inventory = rooms[args[0]].bookshelves[args[1]].rows[args[2]].GetInventory(); break;
                default: 
                    inventory = items;
                    break;
            }
            
            return inventory;
        }
    }
}
