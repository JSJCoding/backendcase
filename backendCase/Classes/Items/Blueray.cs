namespace backendCase
{
    public class Blueray : Item
    {
        public List<Track> tracks;
        public Blueray(int ISBN, string title, List<Track> tracks) : base(ISBN, title)
        {
            this.tracks = tracks;
        }

        public override bool ContainsAll(string[] searchTerms)
        {
            string value = title + string.Join(",", tracks);
            foreach(string term in searchTerms)
            {
                if(!value.Contains(term))
                    return false;
            }
            return true;
        }

        public override string ToString()
        {
            string result = $"{this.GetType().Name} - ";
            result += $"Title: \"{title}\", Tracks: ";
            foreach (Track track in tracks)
               result += $"{track}"; 

            return result;
        }
    }
}