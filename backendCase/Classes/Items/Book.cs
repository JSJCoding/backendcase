namespace backendCase
{
    public class Book : Item
    {
        public List<string> authors = new List<string>();
        public string publisher;
        public int publicationYear;
        public int numberOfPages;
        public Book(int ISBN, string title, List<string> authors, string publisher, int published, int numberOfPages) : base(ISBN, title)
        {
            this.authors = authors;
            this.publisher = publisher;
            this.publicationYear = published;
            this.numberOfPages = numberOfPages;
        }

        public override bool ContainsAll(string[] searchTerms)
        {
            string value = title + publisher + publicationYear + string.Join(",", authors);
            foreach(string term in searchTerms)
            {
                if(!value.Contains(term))
                    return false;
            }
            return true;
        }

        public override string ToString()
        {
            string result = $"{this.GetType().Name} - Authors: ";

            foreach (string author in authors)
               result += $"{author}, "; 

            result += $"Title: \"{title}\", ";
            result += $"Publisher: {publisher}, ";
            result += $"Published: {publicationYear}, ";
            result += $"NumberOfPages: {numberOfPages}";

            return result;
        }
    }
}