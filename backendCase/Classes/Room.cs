namespace backendCase
{
    public class Room
    {
        public List<Bookshelv> bookshelves = new List<Bookshelv>();
        private int ID;
        private int maxShelvCount; // Question how do we know the number of shelves per room, when the room size may differ?
        private int shelvID = 0;

        public Room(int ID)
        {
            this.ID = ID;
            this.maxShelvCount = 2;
            bookshelves.Add(new Bookshelv(shelvID++));
        }

        public bool AddToInventory(Item item)
        {
            bool status = bookshelves[shelvID-1].AddToInventory(ID, item);
            if(status == false && shelvID < maxShelvCount)
            {
                bookshelves.Add(new Bookshelv(shelvID++));
                status = bookshelves[shelvID-1].AddToInventory(ID, item);
            }
            return status;
        }

        public string GetItemLocation(int ISBN)
        {
            List<Item> inventory = GetInventory();
            Item result = inventory.First(item => item.ISBN == ISBN);

            if(result == null)
                return "No Book Found";

            return result.location;
        }

        public List<Item> GetInventory()
        {
            List<Item> inventory = new List<Item>();
            foreach(Bookshelv bookshelf in bookshelves)
            {
                inventory.AddRange(bookshelf.GetInventory());
            }
            return inventory;
        }
    }
}