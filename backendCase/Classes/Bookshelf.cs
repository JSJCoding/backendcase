namespace backendCase
{
    public class Bookshelv
    {
        public List<Row> rows = new List<Row>();
        private int ID;
        private int maxRowCount; // Question how do we know the number of books per row, when books have different sizes?
        private int rowID = 0;

        public Bookshelv(int ID)
        {
            this.ID = ID;
            this.maxRowCount = 2;
            rows.Add(new Row(rowID++));
        }

        public List<Item> GetInventory()
        {
            List<Item> inventory = new List<Item>();
            foreach(Row row in rows)
            {
                inventory.AddRange(row.GetInventory());
            }
            return inventory;
        }

        public bool AddToInventory(int roomID, Item item)
        {
            bool status = rows[rowID-1].AddToInventory(roomID, ID, item);
            if(status == false && rowID < maxRowCount)
            {
                rows.Add(new Row(rowID++));
                status = rows[rowID-1].AddToInventory(roomID, ID, item);
            }
            return status;
        }
    }
}