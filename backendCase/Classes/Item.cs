namespace backendCase
{
    public abstract class Item
    {
        public int ISBN;
        public string title;
        public string location = "";
        
        public Item(int ISBN, string title)
        {
            this.ISBN = ISBN;
            this.title = title;
        }

        public abstract bool ContainsAll(string[] searchTerms);
        public abstract string ToString();
    }
}