namespace backendCase
{
    public enum TrackType 
    {
        audio,
        video
    }

    public class Track
    {
        public TrackType trackType;
        public string title;
        public string artist;
        public int duration;
        public Track(TrackType trackType, string title, string artist, int duration)
        {
            this.trackType = trackType;
            this.title = title;
            this.artist = artist;
            this.duration = duration;
        }

        public override string ToString()
        {
            string result = $"{this.GetType().Name} - ";
            result += $"Title: \"{title}\", ";
            result += $"Artist: {artist}, ";
            result += $"Duration: {duration}";

            return result;
        }
    }
}