namespace backendCase
{
    public class Row
    {
        public List<Item> items = new List<Item>();
        private int ID;
        private int maxBookCount = 1; // Question how do we know the number of books per row, when books have different sizes?

        public Row(int ID)
        {
            this.ID = ID;
        }

        public List<Item> GetInventory()
        {
            return items;
        }

        public bool AddToInventory(int roomID, int shelfID, Item item)
        {
            if(items.Count == maxBookCount)
                return false;

            item.location = $"{roomID}-{shelfID}-{ID}";
            items.Add(item);
            return true;
        }
    }
}